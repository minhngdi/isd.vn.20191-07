# ISD.VN.20191-07 - Homework Week 8

# Work assignment in Architecture Design AFC

* Leader: Nguyễn Đình Minh

## Đỗ Thúy Nga

### Sửa SDD DataModeling cho:

* Ticket-Card Information
* Ticket

### Vẽ Specification Design Class Diagram

* Enter the platform with one-way ticket

### Viết test code cho SDK

## Nguyễn Trọng Nghĩa

### Sửa SDD DataModeling cho:

* Trip

### Vẽ Specification Design Class Diagram

* Exit the platform area with one-way ticket

* Enter the platform area with 24h ticket

### Sửa lại class diagrams combine

### Viết test code cho SDK

## Vũ Đức Nguyễn

### Sửa SDD DataModeling cho:

* Station
* Prepaid-card

### Vẽ Specification Design Class Diagram

* Enter the platform area with prepaid card

### Viết test code cho SDK

## Đỗ Quốc Nam

### Vẽ Specification Design Class Diagram

* Exit the platform area with 24h ticket

### Viết test code cho SDK

## Nguyễn Đình Minh

### Viết test code cho SDK

### Vẽ Specification Design Class Diagram

* Exit the platform area with prepaid card

### Tổng hợp bài tập tuần vào folder #TeamCombine (SDD DataModeling)

### Combined class diagram cho toàn hệ thống
