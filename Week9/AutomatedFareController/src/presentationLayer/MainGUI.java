package presentationLayer;

import java.util.Scanner;

public class MainGUI {
    public static String mainScreen() {
        System.out.println("These are stations in the line M14 of Paris:");
        System.out.print("a. Saint-Lazare\n " +
                "b. Madeleine\n" + 
                "c. Pyramides\n" + 
                "d. Chatelet\n" + 
                "e. Gare de Lyon\n" + 
                "f. Bercy\n" + 
                "g. Cour Saint-Emilion\n" + 
                "h. Bibliotheque Francois Mitterrand\n" + 
                "i. Olympiades\n");
        System.out.println("Available actions: 1-enter station, 2-exit station\n");
        System.out.println("You can provide a combination of number (1 or 2) and a letter from (a to i) to enter or exit a station (using hyphen in between). For instance, the combination “2-d” will bring you to exit the station Chatelet.");
        System.out.print("Your input: ");
        final Scanner scanner = new Scanner(System.in);
        final String input = scanner.nextLine();
        return input;
    }

    public static String checkEnterExit(final String inputMainScreen) {
        if (inputMainScreen.charAt(0) == '1') {
            return "enter station";
        } else if (inputMainScreen.charAt(0) == '2') {
            return "exit station";
        } else {
            return "invalid number for enter/exit";
        }
    }

    // check symbol '-'
    public static boolean checkInput(final String inputMainScreen) {
        if (inputMainScreen.charAt(1) == '-') {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkStation(final String inputMainScreen) {
        if (inputMainScreen.charAt(2) >= 'a' && inputMainScreen.charAt(2) <= 'i') {
            return true;
        } else {
            return false;
        }
    }

    public static String saveStation(final char c) {
        if (c == 'a') {
            return "Saint-Lazare";
        } else if (c == 'b') {
            return "Madeleine";
        } else if (c == 'c') {
            return "Pyramides";
        } else if (c == 'd') {
            return "Chatelet";
        } else if (c == 'e') {
            return "Gare de Lyon";
        } else if (c == 'f') {
            return "Bercy";
        } else if (c == 'g') {
            return "Cour Saint-Emilion";
        } else if (c == 'h') {
            return "Bibliotheque Francois Mitterrand";
        } else if (c == 'i') {
            return "Olympiades";
        }
        return "invalid station";
    }

    public static String choosingATicketCard() {
        System.out.println("Choosing a ticket/card: ");
        System.out.println("These are exsting tickets/cards: ");
        // Todo
        System.out.print("Please provide the ticket code you want to enter/exit: ");
        final Scanner scanner = new Scanner(System.in);
        final String input = scanner.nextLine();
        return input;
    }

    public static void main(final String[] args) {

        String choosingATicketCard, station;
        int checkEnterExit = 0;
        boolean checkInput, checkStation;
        final String inputMainScreen = mainScreen();
        
        // xu ly inputMainScreen
        if(inputMainScreen.length() < 3) {
            System.out.println("Systax Error");
            return;
        }
//        checkEnterExit = checkEnterExit(inputMainScreen);
//        checkInput = checkInput(inputMainScreen);
//        checkStation = checkStation(inputMainScreen);
//        if (checkEnterExit == 0 || checkInput == false || checkStation == false) {
//            System.out.println("Systax Error");
//            return;
//        }
//        station = saveStation(inputMainScreen.charAt(2));
//        switch (checkEnterExit) {
//        case 1:
//            System.out.println("Enter-Station: " + station);
//            choosingATicketCard = choosingATicketCard();
//            System.out.println(choosingATicketCard);
//            // Todo
//            break;
//        case 2:
//            System.out.println("Exit-Station: " + station);
//            choosingATicketCard = choosingATicketCard();
//            System.out.println(choosingATicketCard);
//            // Todo
//            break;
//        default:
//            System.out.println("Systax Error");
//            break;
//        }
    }
}
