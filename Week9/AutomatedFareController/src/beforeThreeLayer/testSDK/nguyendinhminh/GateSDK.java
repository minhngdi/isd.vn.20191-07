package beforeThreeLayer.testSDK.nguyendinhminh;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.gate.Gate;

public class GateSDK {
    public static void main(String[] args) throws InvalidIDException {
        Gate gate = Gate.getInstance();
        gate.open();
        gate.close();
    }
}
