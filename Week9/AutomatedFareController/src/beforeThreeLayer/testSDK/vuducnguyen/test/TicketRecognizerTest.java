package beforeThreeLayer.testSDK.vuducnguyen.test;

import java.util.Scanner;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

public class TicketRecognizerTest {

	public static void main(String[] args) throws InvalidIDException {
//		String pseudoBarCode = "abcdefgh";
		System.out.print("Nhap vao 1 string 8 ki tu viet thuong: ");
		Scanner scanner = new Scanner(System.in);
		String pseudoBarCode = scanner.nextLine();
		TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
		try {
			String ticketId = ticketRecognizer.process(pseudoBarCode);
			System.out.println(ticketId);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
