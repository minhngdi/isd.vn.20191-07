package beforeThreeLayer.firstWeekCode.ticketCardInfo;

public class TicketCardInfo {
	private String id;
	private int type;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
