package beforeThreeLayer.firstWeekCode.ticketCardInfo.vuducnguyen;


import beforeThreeLayer.firstWeekCode.ticketCardInfo.TicketCardInfo;

public class PrepaidCard extends TicketCardInfo {
	private float balance;

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
}
