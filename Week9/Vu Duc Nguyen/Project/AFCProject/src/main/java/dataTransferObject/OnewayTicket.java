package dataTransferObject;

public class OnewayTicket {
	private int id;
	private String status;
	private float price;
	private String embarkation;
	private String disembarkation;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getEmbarkation() {
		return embarkation;
	}
	public void setEmbarkation(String embarkation) {
		this.embarkation = embarkation;
	}
	public String getDisembarkation() {
		return disembarkation;
	}
	public void setDisembarkation(String disembarkation) {
		this.disembarkation = disembarkation;
	}
	
}
