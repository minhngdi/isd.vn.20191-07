package dataTransferObject;

// high cohesive behavior
class TFid {
	String tfid;
	public String getTFid(String tfid)
	{
		this.tfid = tfid;
		return tfid;
	}
}

class TFstatus {
	String tfstatus;
	public String getTFstatus(String tfstatus)
	{
		this.tfstatus = tfstatus;
		return tfstatus;
	}
}

class TFPrice {
	Float tfprice;
	public Float getTFprice(Float tfprice)
	{
		this.tfprice = tfprice;
		return tfprice;
	}
}

public class TwentyHoursTicket {
    private int tfid;
    private String tfstatus;
    private float tfprice;
    
    public int getTFid() {
    	return tfid;
    }
    
    public String getTFstatus() {
		return tfstatus;
	}
    
	public void setTFstatus(String tfstatus) {
		this.tfstatus = tfstatus;
	}
	
	public float getTFprice() {
		return tfprice;
	}
	public void setTFprice(float tfprice) {
		this.tfprice = tfprice;
	}
}
