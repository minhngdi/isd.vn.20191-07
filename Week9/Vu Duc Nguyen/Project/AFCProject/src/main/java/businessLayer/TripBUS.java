package businessLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataLayer.ConnectionUtils;
import dataLayer.TripDAO;
import dataTransferObject.Trip;

public class TripBUS {
	public static int createNewTrip(String idTicketCard, int departureStationID) throws ClassNotFoundException, SQLException {
		return TripDAO.createNewTrip(idTicketCard, departureStationID);
	}
	
	public static int updateTrip(int id, int arrivalStationID, double price) throws ClassNotFoundException, SQLException {
		return TripDAO.updateTrip(id, arrivalStationID, price);
	}
	
	public static ArrayList<Trip> getTripByTicketCardID (String ticketCardID) throws ClassNotFoundException, SQLException {
		return TripDAO.getTripByTicketCardID(ticketCardID);
	}
	
	public static Trip getTripById(Integer id) throws ClassNotFoundException, SQLException {
		return TripDAO.getTripById(id);
	}
}
