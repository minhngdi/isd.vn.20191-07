package dataTransferObject;

public class OnewayTicket {
	private String id;
	private int status;
	private double price;
	private int embarkation;
	private int disembarkation;
	private String code;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getEmbarkation() {
		return embarkation;
	}
	public void setEmbarkation(int embarkation) {
		this.embarkation = embarkation;
	}
	public int getDisembarkation() {
		return disembarkation;
	}
	public void setDisembarkation(int disembarkation) {
		 this.disembarkation = disembarkation;
	}
	
}
