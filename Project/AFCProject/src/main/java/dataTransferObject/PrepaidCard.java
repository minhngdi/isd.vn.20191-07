package dataTransferObject;

public class PrepaidCard extends Card{
	private String code;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
