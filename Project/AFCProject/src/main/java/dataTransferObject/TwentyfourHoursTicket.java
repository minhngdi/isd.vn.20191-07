package dataTransferObject;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

public class TwentyfourHoursTicket extends Ticket {
	private String status;
	private Timestamp startTime;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Timestamp getStartTime() {
		return startTime;
	}
	
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
}
