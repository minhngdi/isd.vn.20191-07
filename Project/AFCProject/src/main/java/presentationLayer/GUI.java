package presentationLayer;

import businessLayer.*;
import businessLayer.card.PrepaidCardBUS;
import businessLayer.ticket.TicketBUS;
import dataLayer.OnewayTicketDAO;
import dataLayer.PrepaidCardDAO;
import dataLayer.StationDAO;
import dataLayer.TwentyfourHoursTicketDAO;
import dataTransferObject.OnewayTicket;
import dataTransferObject.PrepaidCard;
import dataTransferObject.TwentyfourHoursTicket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import hust.soict.se.customexception.InvalidIDException;

/**
 * @author Admin
 *
 */
/**
 * @author Admin
 *
 */
public class GUI {
	/**
	 * @return
	 * @throws InvalidIDException
	 */
	public static String mainScreen() throws InvalidIDException {
		System.out.println("ponmlkji processed code: "+TicketRecognizerInterface.process("ponmlkji")+".");
		System.out.println("These are stations in the line M14 of Paris");
		System.out.print("a. Saint-Lazare\n" +
				"b. Madeleine\n" + 
				"c. Pyramides\n" + 
				"d. Chatelet\n" + 
				"e. Gare de Lyon\n" + 
				"f. Bercy\n" + 
				"g. Cour Saint-Emilion\n" + 
				"h. Bibliotheque Francois Mitterrand\n" + 
				"i. Olympiades\n");
		System.out.println("Available actions: 1-enter station, 2-exit station");
		System.out.println("You can provide a combination of number (1 or 2) \n"
				+ "and a letter from (a to i) to enter or exit a station (using hyphen in between).\n"
				+ "For instance, the combination 2-d will bring you to exit the station Chatelet.");
		System.out.print("Your input: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	
	public static void clrscr() {
		// todo
	}
	
	public static void pressAnyKeyToContinue()
	 { 
		System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        } catch(Exception e) {
        	
        }
	 }
	
	/**
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public static String choosingATicketCard() throws ClassNotFoundException, SQLException {
		clrscr();
		System.out.println("Choosing a ticket/card:");
		System.out.println("These are exsting tickets/cards:");
//		System.out.println("abcdefgh: One-way ticket between Chatelet and Olympiades: New â€“ 3.42 euro");
//		System.out.println("ijklmnop: 24h tickets: New");
//		System.out.println("ABCDEFGH: Prepaid card: 5.65 euros");
//		System.out.println("ponmlkji: 24h tickets: Valid until 14:05 - 4th of December, 2019");
		readFile();
		System.out.print("Please provide the ticket code you want to enter/exit: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	
	/**
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException 
	 */
	public static void start() throws InvalidIDException, ClassNotFoundException, SQLException, IOException {
		
		PrepaidCardBUS prepaidCardBUS = PrepaidCardBUS.getInstance();
		int stationID=0;
		String  pseudoBarCode = null, ticketCode = null;
		while (true) {
			String inputMainScreen = mainScreen();
			
			// xu ly inputMainScreen
			if(inputMainScreen.length() < 3) {
				System.out.println("Systax Error");
				return;
			}
			
			if (CheckInput.checkEnterExit(inputMainScreen) == 0 || 
				CheckInput.checkInput(inputMainScreen) == false ||
				CheckInput.checkStation(inputMainScreen) == false) {
				System.out.println("Systax Error");
				return;
			}
			stationID = inputMainScreen.charAt(2)-'a'+1;
			switch (CheckInput.checkEnterExit(inputMainScreen)) {
			case 1:
				pseudoBarCode = choosingATicketCard();
				if( CheckInput.checkPseudoBarCode(pseudoBarCode) == -1) {
					System.out.println("Error lengths or number");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 0) {
					System.out.println("Error pseudoBarCode");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 1) {
					// handle Card
					prepaidCardBUS.handleCardWhenEnterStation(pseudoBarCode, stationID);
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 2) {
					// handle Ticket
					//System.out.println("Ticket");
					TicketBUS.handleTicketWhenEnterStation(pseudoBarCode, stationID);
				}
				pressAnyKeyToContinue();
				break;
			case 2:
				pseudoBarCode = choosingATicketCard();
				System.out.println(pseudoBarCode);
				if( CheckInput.checkPseudoBarCode(pseudoBarCode) == -1) {
					System.out.println("Error lengths or number");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 0) {
					System.out.println("Error pseudoBarCode");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 1) {
					// handle Card
					prepaidCardBUS.handleCardWhenExitStation(pseudoBarCode, stationID);
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 2) {
					// handle Ticket
					TicketBUS.handleTicketWhenExitStation(pseudoBarCode, stationID);
				}
				pressAnyKeyToContinue();
				break;
			default:
				System.out.println("Systax Error");
				break;
			}
		}
		
	}
	
	/**
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * 
	 */
	public static void readFile() throws ClassNotFoundException, SQLException {
		BufferedReader br = null;
		DecimalFormat df = new DecimalFormat("#.00");
        try {   
            br = new BufferedReader(new FileReader("C:\\Users\\Admin\\Desktop\\isd.vn.20191.07.fork\\Project\\Database\\information.txt"));
            
            
            String textInALine="";
            
           HashMap<String, String> map = new HashMap<String, String>();
            while ((textInALine = br.readLine()) != null) {
            	String array1[] = textInALine.split(":");
                System.out.println(textInALine);
                map.put(array1[0], array1[1]);
                //textInALine = br.readLine();
            }
            Set<Map.Entry<String, String>> set = map.entrySet();
            OnewayTicket owTicket;
            TwentyfourHoursTicket thTicket;
            PrepaidCard card;
            for(String id :map.keySet()) {
            	String type=map.get(id);
            	if(type=="OW") {
            		owTicket=OnewayTicketDAO.findbyId(id);
            		System.out.println(id+":  One-way ticket between "+StationDAO.getStationNameById(owTicket.getEmbarkation())
            							+ " and " + StationDAO.getStationNameById(owTicket.getDisembarkation())
            							+ "  "+ df.format(owTicket.getPrice())+" eur");
            	}
            	if(type=="TH") {
            		thTicket=TwentyfourHoursTicketDAO.findbyId(id);
            		System.out.println(id+": 24h ticket");
            	}
            	if(type=="PC") {
            		card=PrepaidCardDAO.getInstance().getCardById(id);
            		System.out.println(id + " :Prepaid card " +card.getBalance());
            	}
            }
         

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
	}
	
	/**
	 * @param id
	 * @param name
	 * @throws IOException
	 */
	public static void writeFile(String id,String name) throws IOException {
		FileWriter writer = new FileWriter("C:\\Users\\Admin\\Desktop\\isd.vn.20191.07.fork\\Project\\Database\\information.txt");
        BufferedWriter buffer = new BufferedWriter(writer);
        buffer.write(id +":" +name);
        buffer.close();
	}
}
