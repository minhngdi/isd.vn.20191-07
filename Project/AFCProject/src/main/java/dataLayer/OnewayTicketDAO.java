package dataLayer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.OnewayTicket;

public class OnewayTicketDAO {
	public static ArrayList<OnewayTicket> onewayTicketAll() throws ClassNotFoundException, SQLException{
		ArrayList<OnewayTicket> arr = new ArrayList<OnewayTicket>();
        String sql = "select * from onewayticket";
     // Láº¥y ra Ä‘á»‘i tÆ°á»£ng Connection káº¿t ná»‘i vÃ o DB.
        Connection connection = ConnectionUtils.getMyConnection();
   
        // Táº¡o Ä‘á»‘i tÆ°á»£ng Statement.
        Statement statement = connection.createStatement();
      
        ResultSet rs = statement.executeQuery(sql);
     // Duyá»‡t trÃªn káº¿t quáº£ tráº£ vá»�.
        while (rs.next()) {// Di chuyá»ƒn con trá»� xuá»‘ng báº£n ghi káº¿ tiáº¿p.
        	
        	OnewayTicket oneway = new OnewayTicket();
            oneway.setId(rs.getString("Id"));
            oneway.setStatus(rs.getInt("status"));
            oneway.setPrice(rs.getFloat("price"));
            oneway.setEmbarkation(rs.getInt("embarkation"));
            oneway.setDisembarkation(rs.getInt("disembarkation"));
            arr.add(oneway);
      
        }
        // Ä�Ã³ng káº¿t ná»‘i
        connection.close();
        return arr;
	}
	
	public static OnewayTicket findbyId(String idOneway) throws ClassNotFoundException, SQLException {
		OnewayTicket owTicket = new OnewayTicket();
		String sql = "SELECT * from onewayticket WHERE Id="+"\""+idOneway +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			owTicket.setId(rs.getString("Id"));
			owTicket.setStatus(rs.getInt("status"));
			owTicket.setPrice(rs.getFloat("price"));
			owTicket.setEmbarkation(rs.getInt("embarkation"));
			owTicket.setDisembarkation(rs.getInt("disembarkation"));
			owTicket.setCode(rs.getString("Code"));
        }
		connection.close();
		return owTicket;
	}

	public static OnewayTicket getByCode(String code) throws ClassNotFoundException, SQLException {
		OnewayTicket owTicket = new OnewayTicket();
		String sql = "SELECT * from onewayticket WHERE Code="+"\""+code +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			owTicket.setId(rs.getString("Id"));
			owTicket.setStatus(rs.getInt("status"));
			owTicket.setPrice(rs.getFloat("price"));
			owTicket.setEmbarkation(rs.getInt("embarkation"));
			owTicket.setDisembarkation(rs.getInt("disembarkation"));
			owTicket.setCode(rs.getString("Code"));
			}
		connection.close();
		return owTicket;
	}
}
