package dataLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.PrepaidCard;

public class PrepaidCardDAO {
	PrepaidCard card;
	private static PrepaidCardDAO instance;
	private PrepaidCardDAO(){}
	
	public static PrepaidCardDAO getInstance(){
		if(instance == null){
            instance = new PrepaidCardDAO();
        }
        return instance;
	}
	
	public ArrayList<PrepaidCard> getAllPrepaidCard() throws ClassNotFoundException, SQLException {
		ArrayList<PrepaidCard> allCards = new ArrayList<PrepaidCard>();
		String sql = "select * from prepaidcard";
		
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
        	card = new PrepaidCard();
        	card.setId(rs.getString("Id"));
        	card.setBalance(rs.getDouble("Balance"));
        	card.setCode(rs.getString("Code"));
        	allCards.add(card);
        }
		connection.close();
		return allCards;
	}
	
	// Lấy thông tin của card bang id
	public PrepaidCard getCardById(String id) throws ClassNotFoundException, SQLException {
		card = new PrepaidCard();
		String sql = "SELECT * from prepaidcard WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	card.setId(rs.getString("Id"));
        	card.setBalance(rs.getDouble("Balance"));
        	card.setCode(rs.getString("Code"));
        }
		connection.close();
		return card;
	}
	
	// Lấy thông tin của card bang code
		public PrepaidCard getCardByCode(String code) throws ClassNotFoundException, SQLException {
			card = new PrepaidCard();
			String sql = "SELECT * from prepaidcard WHERE Code="+"\""+code +"\"";
			Connection connection = ConnectionUtils.getMyConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()) {
	        	card.setId(rs.getString("Id"));
	        	card.setBalance(rs.getDouble("Balance"));
	        	card.setCode(rs.getString("Code"));
	        }
			connection.close();
			return card;
		}
	public boolean updateBalance(String id, double balance) throws ClassNotFoundException, SQLException {
		String sql = "UPDATE prepaidcard set Balance="+balance+" WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		boolean check = statement.execute(sql);
		return check;
	}
}
