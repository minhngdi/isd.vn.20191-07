package businessLayer.ticket;

import java.sql.SQLException;
import java.text.DecimalFormat;

import businessLayer.TripBUS;
import dataLayer.OnewayTicketDAO;
import dataLayer.StationDAO;
import dataLayer.TripDAO;
import dataTransferObject.OnewayTicket;
import dataTransferObject.Trip;
import hust.soict.se.customexception.InvalidIDException;
import presentationLayer.GateInterface;
import presentationLayer.TicketRecognizerInterface;

public class OnewayTicketBUS {
	
	public static OnewayTicket getTicketById(String id) throws ClassNotFoundException, SQLException {
		return OnewayTicketDAO.findbyId(id);
	}
	
	public static OnewayTicket getTicketByCode(String code) throws ClassNotFoundException, SQLException {
		return OnewayTicketDAO.getByCode(code);
	}
	
	public static int checkEmbarkation(int StationID,OnewayTicket owTicket) throws ClassNotFoundException, SQLException {
		Double cmp=StationDAO.getDistanceById(StationID);
		Double enter=StationDAO.getDistanceById(owTicket.getEmbarkation());
		Double exit=StationDAO.getDistanceById(owTicket.getDisembarkation());
		if(cmp>=0 && cmp>=enter && cmp<=exit) return 1;
		return 0;

	}
	
	public static int checkDisembarkation(int stationID,OnewayTicket owTicket) throws ClassNotFoundException, SQLException {
		Double cmp=StationDAO.getDistanceById(stationID);
		int x=checkEmbarkation(stationID,owTicket);
		int enterStationID;
		enterStationID=owTicket.getDisembarkation();
		if(enterStationID!=0) {
			double price=caculateOnewayTicket(enterStationID,stationID,owTicket);
			double distance=StationDAO.getDistanceById(enterStationID);
			if(distance < cmp && price >=0 && x==1) return  1;
		}		
		return 0;
	}
	
	public static double caculateOnewayTicket(int enterStationID,int exitStationID,OnewayTicket owTicket)
			throws ClassNotFoundException, SQLException {
		Double enter=StationDAO.getDistanceById(enterStationID);
		Double exit=StationDAO.getDistanceById(exitStationID);
		Double distance=exit-enter;
		Double price=owTicket.getPrice();
		Double fare=(double) 0;
		if(distance<=5) 
			fare=1.9;
		else {
			fare=1.9;
			Double distance1=distance-5;
			while(distance1-2>=0) {
				fare+=0.4;
				distance1=distance1-2;
			}
		}
		owTicket.setPrice(price-fare);
		return owTicket.getPrice();
	}
	//Use Strategy for caculate Ticket
	public static void displayOnewayTicketInfo(OnewayTicket owTicket) throws ClassNotFoundException, SQLException {
		DecimalFormat df = new DecimalFormat("#.00");
		System.out.println("\t\tONE-WAY TICKET");
		System.out.println("\t\t"+owTicket.getId());
		int enterStationID=owTicket.getEmbarkation();
		int exitStationID=owTicket.getDisembarkation();
		if(checkEmbarkation(enterStationID, owTicket)==0)
			System.out.println("Invalid oneway ticket");
		else if(checkDisembarkation(exitStationID, owTicket)==0) {
			System.out.println("Invalid oneway ticket");
			System.out.println("Not enough money");
		}
		else {
			String embarkation=StationDAO.getStationNameById(owTicket.getEmbarkation());
			String disembarkation=StationDAO.getStationNameById(owTicket.getDisembarkation());
			System.out.println(embarkation + "\t"+ df.format(owTicket.getPrice())+" eur");
			System.out.println(disembarkation);
		}
		
	}
	
//	public static void main(String[] args) throws ClassNotFoundException, SQLException {
//		if(ITicketBUS.getTicketType("07c84c6c4ba59f88")==2) System.out.println("OnewayTicket");
//		else System.out.println("1");
//	}
	public static void handleOnewayTicketWhenEnterStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		//Onewayticket
		OnewayTicket owTicket = new OnewayTicket();
		owTicket = getTicketByCode(ticketCode);
		if( owTicket.getId() == null) {
			System.out.println("Oneway Ticket does not exist");
			GateInterface.close();
//		} else if( TripBUS.getTripByTicketCardID(owTicket.getId()).size() > 0) {
//			System.out.println("Oneway Ticket is using");
		}else if( checkEmbarkation(stationID,owTicket) ==1) {
			GateInterface.open();
			OnewayTicketBUS.displayOnewayTicketInfo(owTicket);
			TripBUS.createNewTrip(owTicket.getId(), stationID);
		}else {
			System.out.println("Invalid Oneway Ticket");
			OnewayTicketBUS.displayOnewayTicketInfo(owTicket);
			System.out.println("Not enough money");
		}
	}

	public static void handleOnewayTicketWhenExitStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		
		//Onewayticket
		OnewayTicket owTicket = new OnewayTicket();
		owTicket = OnewayTicketBUS.getTicketByCode(ticketCode);
		if( owTicket.getId() == null) {
			System.out.println("Oneway Ticket does not exist");
			GateInterface.close();
//		} else if( TripBUS.getTripByTicketCardID(owTicket.getId()).size() > 0) {
//			System.out.println("Oneway Ticket is using");
		}else if( OnewayTicketBUS.checkDisembarkation(stationID,owTicket) ==1) {
			Trip trip = TripBUS.getTripByTicketCardID(owTicket.getId()).get(0);
			GateInterface.open();
			OnewayTicketBUS.displayOnewayTicketInfo(owTicket);
			double price=caculateOnewayTicket(trip.getDepartureStationID(), stationID, owTicket);
			TripBUS.updateTrip(trip.getId(), stationID, price);
		}else {
			System.out.println("Invalid Oneway Ticket");
			OnewayTicketBUS.displayOnewayTicketInfo(owTicket);
			
		}
	}
}
