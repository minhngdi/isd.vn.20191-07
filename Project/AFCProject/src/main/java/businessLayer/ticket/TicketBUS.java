package businessLayer.ticket;

import java.sql.SQLException;

import dataLayer.TicketDAO;
import dataTransferObject.Ticket;
import hust.soict.se.customexception.InvalidIDException;
import presentationLayer.TicketRecognizerInterface;

public class TicketBUS {
	public static int getTicketType(String code) throws ClassNotFoundException, SQLException {
		Ticket ticket=TicketDAO.findbyCode(code);
		return ticket.getType();
	}
	
	public static void handleTicketWhenEnterStation(String pseudoBarCode,int stationID) 
			throws InvalidIDException, ClassNotFoundException, SQLException, NullPointerException {
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		if(getTicketType(ticketCode)==1)
			OnewayTicketBUS.handleOnewayTicketWhenEnterStation(pseudoBarCode, stationID);
		else if(getTicketType(ticketCode)==2) {
			//handle 24h  ticket
			System.out.println("Enter with 24 HOUR Ticket");
			TwentyfourHoursTicketBUS.processTfhTicket(pseudoBarCode);
		}
		else System.out.println("Invalid Ticket code");
	}
	
	public static void handleTicketWhenExitStation(String pseudoBarCode,int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		if(getTicketType(ticketCode)==1)
			OnewayTicketBUS.handleOnewayTicketWhenExitStation(pseudoBarCode, stationID);
		else if(getTicketType(ticketCode)==2) {
			//handle 24h  ticket
			System.out.println("Exit with 24 HOUR Ticket");
			TwentyfourHoursTicketBUS.processTfhTicketExit(pseudoBarCode);
		}
		else System.out.println("Invalid Ticket code");
	}
}