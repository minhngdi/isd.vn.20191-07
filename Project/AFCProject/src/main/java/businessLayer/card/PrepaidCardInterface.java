package businessLayer.card;

import java.sql.SQLException;

import hust.soict.se.customexception.InvalidIDException;

public interface PrepaidCardInterface {
	public double updateBalance(String id, double calculateFee);
	public void handleCardWhenEnterStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException;
	public void handleCardWhenExitStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException;
}
