package businessLayer.card;
import java.sql.SQLException;

import dataTransferObject.Card;

public interface CardInterface {
	public double getCardBalance(String id);	
	public boolean checkBalanceFirst(String id);	
	public boolean checkBalanceWhenExit (String id);	
	public double calculateFee(int idStation1, int idStation2) throws ClassNotFoundException, SQLException;	
	public void displayCardInfor (Card card);
}
