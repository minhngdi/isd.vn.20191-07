//package junitTest;
//
//import businessLayer.card.*;
//
//import org.junit.Before;
//import org.junit.jupiter.api.Test;
//
//public class PrepaidCardBUSTest {
//	
//	private PrepaidCardBUS prepaidCardBUSTest;
//	@Before
//	public void setUp() throws Exception {
//		prepaidCardBUSTest = new PrepaidCardBUS();
//	}
//
//	@Test
//	public void testGetCardBalance() {
//		double balance = PrepaidCardBUS.getCardBalance("2345");
//		double result = 25;
//		assertEquals(result, balance, 0);
//	}
//
//	@Test
//	public void testCheckBalanceFirst() {
//		boolean check1 = prepaidCardBUSTest.checkBalanceFirst("2345");
//		assertEquals(true, check1);
//		
//		boolean check2 = prepaidCardBUSTest.checkBalanceFirst("4444");
//		assertEquals(false, check2);
//	}
//
//	@Test
//	public void testUpdateBalance() {
//		double balance = prepaidCardBUSTest.updateBalance("4444", 2.5);
//		double result = -1;
//		assertEquals(result, balance, 0);
//	}
//
//	@Test
//	public void testCheckBalanceWhenExit() {
//		boolean check1 = prepaidCardBUSTest.checkBalanceFirst("2345");
//		assertEquals(true, check1);
//		
//		boolean check2 = prepaidCardBUSTest.checkBalanceFirst("4444");
//		assertEquals(false, check2);
//	}
//
//}
