ISD.VN.20191-07
Tổng hợp review
----
Reviewer: Nguyễn Trọng Nghĩa
review for: Đỗ Thúy Nga
Content:
  1. Nên để khoảng cách giữa các () , : vv.
  2. Ticket recognizer action: kiểm tra loại vé
  3. Nên chuyển thành System bởi vì Ticket Recognizer chỉ có nhiệm vụ kiểm tra loại vé thôi.
  Các thao tác phía sau kiểm tra vé là của hệ thống mất rồi.
  Cái alternative t nghĩ nên ghi thêm là trả lại và thông báo tình trạng của vé nhé.
  Mục đích của Check information of prepaid card when enter station không phải là nhập điểm đến mà là đi vào trạm nhé theo t là như vậy
----
Reviewer: Vũ Đức Nguyễn
review for Nguyễn Đình Minh
Content:
  Về cơ bản là khá ok, chỉ còn thiếu một số sự kiện sau.
  1. Về luồng sự kiện của Check information of prepaid card when enter station.
	  - Thiếu phần kiểm tra id của thẻ.
	  - Thiếu hiện thị thông tin thẻ và hiển thị "Opening card" khi thông tin kiểm tra là hợp lệ
	  - Thiếu lưu trữ nhà ga hành khách lên, cần phải lưu trữ nhà ga hành khách lên để sau khi xuống ga có thể tính toán được số tiền.
	  - Không có điều khiển cửa, mà hệ thống sẽ thay đổi biến trạng thái của cửa và gửi dữ liệu cho gate. phần điều khiển cửa thuộc về use case "control gate"
  2. Về luồng sự kiện của Check information of prepaid card when exit station.
	  - Thiếu phần kiểm tra id của thẻ.
	  - Thiếu hiện thị thông tin thẻ và hiển thị "Opening card" khi thông tin kiểm tra là hợp lệ
----
Reviewer: Nguyễn Đình Minh
review for Nguyễn Trọng Nghĩa
Content:
- Thêm MSSV ở header.
- In each use case description:
    1. Lack of Precondition and Trigger of usecase.
    2. Mô-tả-chung quá dài, nên tách ra thành nhiều ý chính riêng.
    3. Mình nghĩ ở nhà ga, cửa ra và vào được tách biệt nhau
      nên nhiệm vụ của Gate ở ra và vào cũng nên được tách ra.

----
Reviewer:Đỗ Thúy Nga from branch:ngadt
review for Vũ Đức Nguyễn
-Ảnh use case ở đầu chỉ nên có mỗi use case diagram không nên chụp cả màn hình,ảnh cần to hơn để dễ nhìn hơn.
-Ở Exceptional flow of events của use case check information of one-way tickets when enter station nên có thêm nói thêm về trạng thái của cổng là đang đóng.
-Nên viết thêm default trạng thái của cổng là đang đóng.
-Trình bày không nên để bảng ở giữa trang này với trang khác.
----
